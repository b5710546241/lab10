package Test;
import static org.junit.Assert.*;
import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;


public class StackTest {
	
	private Stack stack;
	private int capacity = 2;
	
	@Before
	public void setUp() {
		// set stack type 0 or 1
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack(capacity);
	}
	
	@Test
	public void newStackIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}
	
	@Test(expected=java.util.EmptyStackException.class)
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
	}
	
	@Test
	public void testPeekTwoTimesAreTheSameObject() {
		stack.push("cake");
		stack.push("cookies");
		Object food1 = stack.peek();
		Object food2 = stack.peek();
		assertSame( food1, food2 );
		assertEquals( capacity, stack.size() );
	}
	
	@Test
	public void testPopOneFromOne() {
		stack = StackFactory.makeStack(1);
		stack.push("Blah");
		assertEquals( "Blah", stack.pop() );
		assertEquals( 0, stack.size() );
	}
	
	@Test
	public void testPopThreeFormThree() {
		stack = StackFactory.makeStack(3);
		stack.push("Blah");
		stack.push("blah");
		stack.push("nana");
		stack.pop();
		stack.pop();
		assertEquals( "nana", stack.pop() );
		assertEquals( 0, stack.size() );
	}
	
	@Test(expected=java.lang.IllegalStateException.class)
	public void testPushWhenStackIsFull() {
		stack.push("1");
		stack.push("2");
		assertFalse( stack.isFull() );
		stack.push("3");
		assertFalse( capacity == stack.size() );
	}
	
	@Test(expected=java.lang.IllegalArgumentException.class)
	public void testPushNullParameter() {
		stack.push(null);
	}
	
	@Test
	public void testPeekEmptyStack() {
		assertEquals( null, stack.peek() );
	}
	
	@Test
	public void testStackSize() {
		stack.push("a");
		assertEquals( 1, stack.size() );
	}
	
	@Test
	public void testStackIsFull() {
		stack.push("bee bee");
		assertFalse( stack.isFull() );
	}
	
	@Test
	public void testPutFirstElement() {
		stack.push("palmmy");
		assertEquals( 1, stack.size() );
	}
	
}
